import 'package:flutter/material.dart';
import 'blocs/provider.dart';
import 'screens/counter_page.dart';

class App extends StatelessWidget {
  build(context) {
    return Provider(
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          body: CounterPage(title: 'Flutter Demo Home Page'),
        ),
      ),
    );
  }
}
