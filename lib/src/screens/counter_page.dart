import 'package:flutter/material.dart';
import '../blocs/provider.dart';

class CounterPage extends StatelessWidget {
  CounterPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(title),
      ),
      body: new Center(
        child: StreamBuilder<int>(
          stream: bloc.outCounter,
          initialData: 0,
          builder: (BuildContext context, AsyncSnapshot<int> snapshot){
            return new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  'You have pushed the button this many times:',
                ),
                new Text(
                  '${snapshot.data}',
                  style: Theme.of(context).textTheme.display1,
                ),
              ],
            );
          }
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: (){bloc.incrementCounter(null);},
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ), 
    );
  }
}
