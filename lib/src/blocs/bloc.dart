import 'dart:async';
import 'validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc extends Object with Validators {
  int _counter;
  BehaviorSubject<int> _counterController = BehaviorSubject<int>();
  StreamSink<int> get _inAdd => _counterController.sink;
  Observable<int> get outCounter => _counterController.stream;

  BehaviorSubject _actionController = BehaviorSubject();
  Function(int) get incrementCounter => _actionController.sink.add;

  Bloc(){
    _counter = 0;
    _actionController.stream
                     .listen(_handleLogic);
  }

  void _handleLogic(data){
    _counter = _counter + 1;
    _inAdd.add(_counter);
  }

  void dispose(){
    _actionController.close();
    _counterController.close();
  }
}
